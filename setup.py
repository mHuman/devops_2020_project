#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

ROOT_PACKAGE_NAME = 'bot'

def parse_requirements():
    with open('requirements.txt') as f:
        return f.read().splitlines()


setup(
    name=ROOT_PACKAGE_NAME,
    version='1.0',
    author=['Maxim Machula'],
    packages=find_packages(),
    long_description='Telegram Pyatnaski Bot',
    requirements=parse_requirements()
)
