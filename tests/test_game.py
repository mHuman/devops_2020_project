from __future__ import print_function
from bot.bot import Game15


def test_passing():
    assert (1, 2, 3) == (1, 2, 3)


def test_game():
    game = Game15(3)
    game.create_map(3)
    start_field = [[1, 2, 3], [4, 5, 6], [7, 8, 0]]
    assert game.field == start_field
    game.shuffle()
    assert game.field != start_field


def test_solve():
    game = Game15(3)
    start_field = [[1, 2, 3], [4, 5, 6], [7, 8, 0]]
    moves = {
        'w': 0,
        'd': 1,
        's': 2,
        'a': 3,
    }
    for direction in game.sol:
        game.move(moves[direction])
    assert game.field == start_field
