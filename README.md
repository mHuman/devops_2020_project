# TelegramBotGamePyatnashki
Bot for python course in MIPT
BOT - @max_machula_test_bot

Реализован бот, для игры в пятнашки на доске произвольного размера (2 <= n <= 16). Можно подключится к боту @max_machula_test_bot в телеграмме ввести /help для показа доступных команд, и играть. <br>
Также реализована наивная функция вывода решения текущего пазла.

# Тесты
Тестируем начальное состояние доски, а также адекватность shuffle-а доски. <br>
Тестируем корректную работу комманды решения пазла.

# CI
CI реализован через встроенный CI gitlab-а, уведомления приходят по электронной почте.

# Build Requirements
Ubuntu OS <br>
python 3.6 <br>
pip <br>
pytest==3.10.1 <br>
pytest-cov==2.6.0 <br>
pyTelegramBotAPI==3.7.1 <br>
coverage==4.5.1 <br> 
